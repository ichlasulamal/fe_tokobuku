import React, {useEffect, useState} from 'react'
import {DetailOrder} from '../../Component'
import { useSelector, useDispatch } from "react-redux";
import { getUserCart, getUserData, updateAlamatPengiriman } from "../../store/action/index";
import {Container, Row, Col, Breadcrumb, Button, Modal, Card, Form} from 'react-bootstrap';
import { FaPhoneAlt, FaRegMap, FaRegHospital} from "react-icons/fa";

const axios = require('axios');

        

const AlamatPengiriman = (props) => {

    const {alamat_pengiriman} = props.user
    const {nomor_telepon, kota_kecamatan, kode_pos, alamat} = alamat_pengiriman

    return (
        <div>
            <div style={{display: 'flex'}}>
                <div style={{display: 'flex', flexDirection: 'column',   justifyContent: 'space-between', marginRight: '20px', padding: '10px'}}>
                    <FaRegHospital />
                    <FaRegMap />
                    <FaPhoneAlt />
                </div>
                <div style={{ justifyContent: 'space-between'}}>
                    <p style={{ margin: "5px 0"}}>{kota_kecamatan} {kode_pos}</p>
                    <p style={{ margin: "5px 0"}}>{alamat}</p>
                    <p style={{ margin: "5px 0"}}>+62 {nomor_telepon}</p>
                </div>
                
                <div>
                </div>
            </div>
            
        </div>
    )
}


const Shipment = () => {
	const dispatch = useDispatch();
    const products = useSelector((state) => state.userReducer.userCart);
    const user = useSelector((state) => state.userReducer.userData);
    
    const [nomortelpon, setNomortelpon] = useState('')
    const [kotakecamatan, setKotakecamatan] = useState('')
    const [kodepos, setKodepos ] = useState('')
    const [alamat_penerima, setAlamat] = useState('') 

    const [idProvince, setIdProvince] = useState()
    const [idCity, setIdCity] = useState()
    const [province, setProvince] = useState()
    const [city, setCity] = useState()


    const provinceArray = [
        {
            "province_id": "1",
            "province": "Bali"
        },
        {
            "province_id": "2",
            "province": "Bangka Belitung"
        },
        {
            "province_id": "3",
            "province": "Banten"
        },
        {
            "province_id": "4",
            "province": "Bengkulu"
        },
        {
            "province_id": "5",
            "province": "DI Yogyakarta"
        },
        {
            "province_id": "6",
            "province": "DKI Jakarta"
        },
        {
            "province_id": "7",
            "province": "Gorontalo"
        },
        {
            "province_id": "8",
            "province": "Jambi"
        },
        {
            "province_id": "9",
            "province": "Jawa Barat"
        },
        {
            "province_id": "10",
            "province": "Jawa Tengah"
        },
        {
            "province_id": "11",
            "province": "Jawa Timur"
        },
        {
            "province_id": "12",
            "province": "Kalimantan Barat"
        },
        {
            "province_id": "13",
            "province": "Kalimantan Selatan"
        },
        {
            "province_id": "14",
            "province": "Kalimantan Tengah"
        },
        {
            "province_id": "15",
            "province": "Kalimantan Timur"
        },
        {
            "province_id": "16",
            "province": "Kalimantan Utara"
        },
        {
            "province_id": "17",
            "province": "Kepulauan Riau"
        },
        {
            "province_id": "18",
            "province": "Lampung"
        },
        {
            "province_id": "19",
            "province": "Maluku"
        },
        {
            "province_id": "20",
            "province": "Maluku Utara"
        },
        {
            "province_id": "21",
            "province": "Nanggroe Aceh Darussalam (NAD)"
        },
        {
            "province_id": "22",
            "province": "Nusa Tenggara Barat (NTB)"
        },
        {
            "province_id": "23",
            "province": "Nusa Tenggara Timur (NTT)"
        },
        {
            "province_id": "24",
            "province": "Papua"
        },
        {
            "province_id": "25",
            "province": "Papua Barat"
        },
        {
            "province_id": "26",
            "province": "Riau"
        },
        {
            "province_id": "27",
            "province": "Sulawesi Barat"
        },
        {
            "province_id": "28",
            "province": "Sulawesi Selatan"
        },
        {
            "province_id": "29",
            "province": "Sulawesi Tengah"
        },
        {
            "province_id": "30",
            "province": "Sulawesi Tenggara"
        },
        {
            "province_id": "31",
            "province": "Sulawesi Utara"
        },
        {
            "province_id": "32",
            "province": "Sumatera Barat"
        },
        {
            "province_id": "33",
            "province": "Sumatera Selatan"
        },
        {
            "province_id": "34",
            "province": "Sumatera Utara"
        }
    ]

    const [cityArray, setCityArray] = useState([])
    
    const [modal , setModal] = useState(false)

    useEffect(() => {
		dispatch(getUserCart())
        dispatch(getUserData())

	}, [dispatch]);

    useEffect(() => {
        if(user['alamat_pengiriman']){
            setNomortelpon(user['alamat_pengiriman']['nomor_telepon'])
            setKotakecamatan(user['alamat_pengiriman']['kota_kecamatan'])
            setKodepos(user['alamat_pengiriman']['kode_pos'])
            setAlamat(user['alamat_pengiriman']['alamat'])
        }
    }, [user])

    useEffect(() => {
        console.log('Id Provincy changing', province)
        console.log('Id city changing', city)

    })

    const handleSubmitUpdateAlamat = () => {
        const newAlamat = {
            label_alamat: user['alamat_pengiriman']['label_alamat'],
            nomor_telepon:  nomortelpon,
            kota_kecamatan: kotakecamatan,
            kode_pos: kodepos,
            alamat: alamat_penerima
        }

        console.log(newAlamat)
        dispatch(updateAlamatPengiriman(newAlamat))
        setModal(false)
    }

    return (
        <div>
            <Modal show={modal} >
                <Card style={{padding: '20px'}}>
                    <Card.Header>ALAMAT PENGIRIMAN</Card.Header>
                    <Card.Body>
                        <Form>
                            <Row>
                                <Col>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Nomor Telepon</Form.Label>
                                        <div style={{display: 'flex'}}>
                                            <Form.Control
                                                value={nomortelpon}
                                                onChange={(e) => setNomortelpon(e.target.value)}
                                                type="number"
                                            />
                                        </div>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Kode Pos</Form.Label>
                                        <div style={{display: 'flex'}}>
                                            <Form.Control
                                                value={kodepos}
                                                onChange={(e) => setKodepos(e.target.value)}
                                                type="number"
                                            />
                                        </div>
                                    </Form.Group>
                                </Col>
                            </Row>

                            {/* <Form.Group controlId="formBasicEmail">
                                <Form.Label>Kota atau Kecamatan</Form.Label>
                                <Form.Control
                                    value={kotakecamatan}
                                    onChange={(e) => setKotakecamatan(e.target.value)}
                                    type="text"
                                />
                            </Form.Group> */}

                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Provinsi</Form.Label>

                                <Form.Control as="select" value={province} onChange={(e) => {
										setProvince(e.target.value)
                                        // Fetch Api an then set city 
                                        axios
                                            .get(`https://api.rajaongkir.com/starter/city?province=${province}`,{
                                                headers: {
                                                    key: 'd250cda4c7582091070daf9556d4b40d'
                                                },
                                            })
                                            .then((res) => {
                                                console.log('Result from raja ongkir')
                                                console.log(res.rajaOngkir.results)
                                                setCityArray(res.rajaOngkir.results)
                                            })
                                            .catch(err=>{
                                                console.log(err)
                                            })
                                            .finally(()=>{
                                                console.log('Fetch city in province , to raja ongkir API')
                                            })
									}}>
                                        {provinceArray.map((e,i) =>{
                                            return(
										        <option value={e.province_id}>{e.province}</option>
                                            )
                                        })}
									</Form.Control>
                            </Form.Group>

                            {cityArray.length > 0 && (
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Kota / Kabupaten</Form.Label>
                                    <Form.Control as="select" value={city} onChange={(e) => {
                                        setCity(e.target.value)
                                    }}>
                                        {cityArray.map((e,i) =>{
                                                return(
                                                    <option value={e.city_id}>{e.city_name}</option>
                                                )
                                            })}
                                    </Form.Control>
                            </Form.Group>

                            )}

                            

                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Alamat Penerima</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={2}
                                    value={alamat_penerima}
                                    onChange={(e) => setAlamat(e.target.value) }
                                    type="text"
                                />
                            </Form.Group>
                        </Form>
                        <hr></hr>
                        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                            <Button style={{marginRight: '20px'}} variant="outline-primary" onClick={()=> setModal(false)}>Cancel</Button>
                            <Button variant="primary" onClick={()=> handleSubmitUpdateAlamat() }>Update</Button>
                        </div>
                    </Card.Body>
                </Card>
            </Modal>


            <Container>
                <Row style={{marginBottom: '40px'}}>
                    <Col lg={8}>
                        <Breadcrumb>
                            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                            <Breadcrumb.Item href="/cart">Cart</Breadcrumb.Item>
                            <Breadcrumb.Item active>Shipment</Breadcrumb.Item>
                        </Breadcrumb>
                        <div style={{display: 'flex', justifyContent: 'space-between'}} >
                            <h5 style={{padding: '10px'}}>Alamat Pengiriman</h5>
                            <div>
                                <Button style={{padding:'5px 12px'}} variant="outline-primary" onClick={() => setModal(true)}>Update</Button>
                            </div>
                            
                        </div>
                        {user['name'] && <AlamatPengiriman user={user} />}
                        <hr></hr>
                        
                    </Col>

                    <Col lg={4}>
                        {products ? <DetailOrder products={products} userData={user} /> : <p>Kejap la</p> }
                    </Col>

                </Row>
            </Container>
        </div>
    )
}

export default Shipment
